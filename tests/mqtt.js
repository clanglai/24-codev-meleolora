const mqtt = require('mqtt')

///////////////////////////////////////////
// PARTIE A MODIFIER SELON L'UTILISATEUR //
///////////////////////////////////////////

//Initialisation des paramètres de la connexion MQTT
const pyCard_id  = '70b3d54992cdbdd6'            // Identifiant de votre carte PySense
const applicationName = 'tp-lora-cooc'           // Nom de votre application dans le serveur TTN
const password = 'NNSXS.LDTJFVIAELHH2ZGWSAQOQOVSGWJVJTBWQAVW5MA.2JBE65Y7WETTEYU5TLD4TOYLC6CH3UI3LHG4FSYO4KRH56BS3R6A' // Mot de passe pour votre application TTN
const topic = `v3/${applicationName}@ttn/devices/eui-${pyCard_id}/up` // topic sur lequel TTN publie les données

/////////////////////
// NE PAS MODIFIER //
/////////////////////
const host       = 'eu1.cloud.thethings.network' // Adresse du serveur TTN
const port       = '1883'                        // Port utilisé par le serveur TTN
const connectUrl = `mqtt://${host}:${port}`      // URL pour se connecter au serveur TTN
const clientId   = `mqtt_${Math.random().toString(16).slice(3)}` // Identifiant du client

//Connexion au serveur TTN
const client = mqtt.connect(connectUrl, {
    clientId,
    clean: true,
    connectTimeout: 4000,
    username: applicationName + '@ttn',
    password: password,
    reconnectPeriod: 1000,
})

// Evenement lancé lors de la connexion vers le serveur TTN
client.on('connect', () => {
    console.log('Successfully connected to the TTN server')

    client.subscribe([topic], () => { // On s'abonne à un topic pour permettre de recevoir les messages que le serveur enverra
        console.log('Successfully subscribed to the topic : ' + topic)
    })
})

// Evenement lancé lors de la réception d'un message
client.on('message', (topic, payload) => {
    const message = payload.toString();
    const payloadRegex = /"frm_payload":"(.+?)"/;
    const payloadMatch = message.match(payloadRegex);

    if (!payloadMatch) return  //Si le payload n'est pas trouvé alors la fonction ne va pas plus loin

    const frm_payload = payloadMatch[1]

    //Affichage du message reçu par le serveur
    console.log(atob(frm_payload))
})