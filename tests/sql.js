const sql = require('mssql')

///////////////////////
// DONNES A MODIFIER //
///////////////////////
const login = 'fablab'
const password = 'fablab'
const host_name = "DESKTOP-87MB5S8"
const port = 56211
const table_name = "datas"

///////////////////////
// A NE PAS MODIFIER //
///////////////////////
const config = {
    user: login,
    password: password,
    server: host_name,
    port: port,
    options: {
        trustedConnection: true,
        trustServerCertificate: true,
        instanceName: 'SQLEXPRESS'
    }
}

// Fonction pour se connecter à la base de donnée
async function connect() {
    await sql.connect(config)
        .then(() => {
            console.log("Successfully connected to the SQL database")
            query()
        })
        .catch(() => {
            console.log("Didn't succeed to connect to the SQL database")
        })
}

// Fonction pour effectuer une requête de test
async function query() {
    const result = await sql.query(`SELECT * FROM ${table_name}`)
        .catch(() => {
            console.log("An error has occured during the query of the datas")
        })

    console.log(result)
}

connect()