// Fonction pour extraire une donnée à partir de l'ensemble des données récupérées par le capteur

function extractBrightness(str) {
    const splitStr = str.split(',')

    if (splitStr.length != 8) return null
    return splitStr[0].trim()
}

function extractHumidity(str) {
    const splitStr = str.split(',')

    if (splitStr.length != 8) return null
    return splitStr[1].trim()
}

function extractPressure(str) {
    const splitStr = str.split(',')

    if (splitStr.length != 8) return null
    return splitStr[2].trim()
}

function extractRainfall(str) {
    const splitStr = str.split(',')

    if (splitStr.length != 8) return null
    return splitStr[3].trim()
}

function extractTemperature(str) { 
    const splitStr = str.split(',')

    if (splitStr.length != 8) return null
    return splitStr[4].trim()
}

function extractWindSpeed(str) {
    const splitStr = str.split(',')

    if (splitStr.length != 8) return null
    return splitStr[5].trim()
}

function extractBatteryLevel(str) {
    const splitStr = str.split(',')

    if (splitStr.length != 8) return 0
    return splitStr[6].trim()
}

function extractStationState(str) {
    const splitStr = str.split(',')

    if (splitStr.length != 8) return 0
    return splitStr[7].trim()
}

// Fonction prenant en argument le message complet provenant de TTN, et renvoyant l'heure du message
function extractTimeFromMessage(message) {
    const regex = /(\d{2}):(\d{2}):(\d{2})/; 
    const match = regex.exec(message); // Recherche l'heure dans le message

    if (match) { // Si une correspondance est trouvée
        const hour = match[1]; // Récupère l'heure du message
        const minute = match[2]; // Récupère la minute
        
        // Crée un objet Date pour l'heure extraite du message
        const messageTime = new Date();

        messageTime.setHours(hour);
        messageTime.setMinutes(minute);
        messageTime.setSeconds(0);
        
        // Ajoute 2 heures à l'objet Date pour la mettre dans le bon fuseau horaire
        const newTime = new Date(messageTime.getTime() + 1 * 60 * 60 * 1000);
        
        // Extraire l'heure et les minutes de l'objet modifié
        const newHour = newTime.getHours();
        const newMinute = newTime.getMinutes();
        
        // Formate l'heure et les minutes dans une chaîne HH:MM
        return `${newHour.toString().padStart(2, '0')}:${newMinute.toString().padStart(2, '0')}`;
    }
    return "No time found in message";
}

// Fonction prenant en argument le message complet provenant de TTN, et renvoyant la date du message
function extractDateFromMessage(message) { 
    const regex = /"received_at":"(\d{4})-(\d{2})-(\d{2})T/;
    const match = message.match(regex); // Recherche la date dans le message

    if (match) { // Si une correspondance est trouvée
        const year = match[1]; // récupère l'année du message
        const month = match[2]; // récupère le mois du message
        const day = match[3]; // récupère le jour du message

        // Construit la date sous le format JJ/MM/AAAA
        const date = `${day}/${month}/${year}`;
        return date;
    }
    return 'Aucune date trouvée dans le message.';
}

module.exports = {
    extractDateFromMessage,
    extractTimeFromMessage,
    extractBrightness,
    extractHumidity,
    extractPressure,
    extractRainfall,
    extractTemperature,
    extractWindSpeed,
    extractBatteryLevel,
    extractStationState
}