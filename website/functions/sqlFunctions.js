function addToDatabase(data) {
    const date = data.date
    const time = data.hour

    const brig = data.brightness
    const humi = data.humidity
    const pres = data.pressure
    const rain = data.rainfall
    const temp = data.temperature
    const wind = data.windSpeed

    const batt = data.batteryLevel
    const stat = data.stationState

    const splittedDate = date.split('/')
    const day   = splittedDate[0]
    const month = splittedDate[1]
    const year  = splittedDate[2]

    const splittedTime = time.split(':')
    const hour    = splittedTime[0]
    const minutes = splittedTime[1]
    const seconds = "0"
    const milliseconds = "0"

    // DATETIMEFROMPARTS est une fonction interne de SQL, la documentation se trouve à l'URL suivante :
    // https://learn.microsoft.com/fr-fr/sql/t-sql/functions/datetimefromparts-transact-sql?view=sql-server-ver16
    const dateQuery = `DATETIMEFROMPARTS(${year}, ${month}, ${day}, ${hour}, ${minutes}, ${seconds}, ${milliseconds})`

    // Ajout des données reçues dans la base de données
    return query = `INSERT INTO datas VALUES (${dateQuery}, ${brig}, ${humi}, ${pres}, ${rain}, ${temp}, ${wind}, ${batt}, ${stat})`
}

module.exports = { addToDatabase }