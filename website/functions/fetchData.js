module.exports = {
    fetchAllData: async (sql, table_name) => {
        const all = await sql.query(`SELECT * FROM ${table_name}`)
        .catch(() => {console.log("An error has occured during the query of the data: all")})

        const dateTime = await sql.query(`SELECT dateTime FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: dateTime")})

        const brightness = await sql.query(`SELECT brightness FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: brightness")})

        const humidity = await sql.query(`SELECT humidity FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: humidity")})

        const pressure = await sql.query(`SELECT pressure FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: pressure")})

        const rainfall = await sql.query(`SELECT rainfall FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: rainfall")})

        const temperature = await sql.query(`SELECT temperature FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: temperature")})
            
        const windSpeed = await sql.query(`SELECT windSpeed FROM ${table_name}`)
                .catch(() => {console.log("An error has occured during the query of the data: windSpeed")})

        const batteryLevel = await sql.query(`SELECT batteryLevel FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: batteryLevel")})

        const stationState = await sql.query(`SELECT stationState FROM ${table_name}`)
            .catch(() => {console.log("An error has occured during the query of the data: stationState")})

        const datas = {
            all          : all.recordset,
            dateTime     : dateTime.recordset,
            brightness   : brightness.recordset,
            humidity     : humidity.recordset,
            pressure     : pressure.recordset,
            rainfall     : rainfall.recordset,
            temperature  : temperature.recordset,
            windSpeed    : windSpeed.recordset,
            batteryLevel : batteryLevel.recordset,
            stationState : stationState.recordset,
        }

        return datas
    }
}