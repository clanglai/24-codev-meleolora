function extractDate(dateTime) {
    return dateTime.split("T")[0].slice(1)
}

function extractTime(dateTime) {
    const time = dateTime.split("T")[1]
    return dateTime.split("T")[1].slice(0, -2)
}

module.exports = {extractDate, extractTime}