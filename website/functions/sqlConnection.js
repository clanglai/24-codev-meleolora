// La base de données est rangée de la façon suivante :

// +----------------------------------------------------------------------------------------------------------------+
// | dateTime | brightness | humidity | pressure | rainfall | temperature | windSpeed | batteryLevel | stationState |
// +----------+------------+----------+----------+----------+-------------+-----------+--------------+--------------+
// |   ....   |    ....    |   ....   |   ....   |    ...   |     ....    |    ...    |     ....     |     ....     |
// +----------+------------+----------+----------+----------+-------------+-----------+--------------+--------------+
// C'est important de le savoir lorsque nous voulons ajouter des données, il faut qu'elles soient dans l'ordre

// Configuration de la connexion à la base de données MSSQL
const sql = require('mssql')

////////////////////////////////////
// A MODIFIER SELON L'UTILISATEUR //
////////////////////////////////////
const login = "fablab"              // Login de votre base de donnée
const password = "fablab"           // Mot de passe du login de votre base de donnée
const host_name = "DESKTOP-87MB5S8" // Nom de l'hôte de votre base de donnée
const port = 56211                  // Numéro du port de votre base de donnée

///////////////////////
// A NE PAS MODIFIER //
///////////////////////
const config = {
    user: login,                      // Nom d'utilisateur pour accéder au serveur
    password: password,               // Mot de passe pour accéder au serveur
    server: host_name,                // Adresse du serveur MSSQL
    port: port,                       // Port sur lequel le serveur écoute
    options: {                        // Options dédiées au bon fonctionnement de la connexion
        trustedConnection: true,      // Option pour faciliter la connexion de la part d'un invité
        trustServerCertificate: true, // Option pour permettre au PC invité d'autoriser la connexion
        instanceName: 'SQLEXPRESS'    // Option pour spécifier l'instance du serveur
    }
}

// Fonction pour se connecter à la base de donnée
function connect() {
    sql.connect(config)
        .then(() => {
            console.log("Successfully connected to the SQL database")
        })
        .catch(() => {
            console.log("An error has occured during the connection to the SQL database")
        })
    return sql
}

// Fonction pour se déconnecter de la base de donnée
function disconnect() {
    sql.close()
        .then(() => {
            console.log("SQL database successfully closed")
        })
        .catch(() => {
            console.log("An error has occured during the closing of the SQL database")
        })
}

module.exports = { sql, config, connect, disconnect }