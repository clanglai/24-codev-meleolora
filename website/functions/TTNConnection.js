const functions = require('./mqttFunctions')
const { config } = require('./sqlConnection')
const { addToDatabase } = require('./sqlFunctions')

module.exports = {
    topic: (client, topic) => {
        // Evenement lancé lors de la connexion vers le serveur TTN
        console.log('Successfully connected to the server')
        client.subscribe([topic], () => { // On s'abonne à un topic pour permettre de recevoir les messages que le serveur enverra
            console.log('Successfully subscribed to the topic : ' + topic)
        })
    },

    insert: async (sql, payload) => {
        await sql.connect(config)
        const message = payload.toString()
        console.log("message : " + message)
        const payloadRegex = /"frm_payload":"(.+?)"/; 
        const payloadMatch = message.match(payloadRegex);

        if (!payloadMatch) return  //Si le payload n'est pas trouvé alors la fonction ne va pas plus loin

        // Etapes de parsing pour récupérer toutes les données dont nous avons besoin
        const frm_payload = payloadMatch[1]

        console.log("frm_payload : " + frm_payload)
        console.log("atob : " + atob(frm_payload))

        const date        = functions.extractDateFromMessage(message)
        const heure       = functions.extractTimeFromMessage(message)

        const brig        = functions.extractBrightness(atob(frm_payload))
        const humi        = functions.extractHumidity(atob(frm_payload))
        const pres        = functions.extractPressure(atob(frm_payload))
        const rain        = functions.extractRainfall(atob(frm_payload))
        const temp        = functions.extractTemperature(atob(frm_payload))
        const wind        = functions.extractWindSpeed(atob(frm_payload))

        const batt        = functions.extractBatteryLevel(atob(frm_payload))
        const stat        = functions.extractStationState(atob(frm_payload))

        // Stockage de ces données dans un dictionnaire
        donnees = {
            "date"         : date,
            "hour"         : heure,
            "brightness"   : brig,
            "humidity"     : humi,
            "pressure"     : pres,
            "rainfall"     : rain,
            "temperature"  : temp,
            "windSpeed"    : wind,
            "batteryLevel" : batt,
            "stationState" : stat
        }

        // Ajout de ces données dans la base de données SQL
        console.log(donnees)

        if (atob(frm_payload).split(',').length != 8) return

        query = addToDatabase(donnees)
        await sql.query(query)
            .then(() => {sql.close()})
    }
}