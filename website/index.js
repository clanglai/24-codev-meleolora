const express = require("express");
const mqtt = require('mqtt')
const path = require("path");

let { sql, config } = require('./functions/sqlConnection.js')
const { fetchAllData } = require('./functions/fetchData.js')
const ttn = require('./functions/TTNConnection.js')
const extraction = require('./functions/extractData')
const functions = require('./functions/functions.js')

const app = express();

// On définit le chemin vers le dossier "views" pour la partie front
app.set("views", path.join(__dirname, "views"));

// On définit notre fichier front comme étant un fichier .ejs
app.set("view engine", "ejs");

// Middleware pour servir les fichiers statiques (comme les fichiers HTML, CSS, etc.)
app.use(express.static(__dirname + '/public'));

//Initialisation des paramètres de la connexion MQTT
const host       = 'eu1.cloud.thethings.network' // Adresse du serveur TTN
const port       = '1883'                        // Port utilisé par le serveur TTN
const clientId   = `mqtt_${Math.random().toString(16).slice(3)}` // Identifiant du client
const connectUrl = `mqtt://${host}:${port}`      // URL pour se connecter au serveur TTN

////////////////////////////////////
// A MODIFIER SELON L'UTILISATEUR //
////////////////////////////////////

const pyCard_id  = '70b3d54992cdbdd6'            // Identifiant de notre carte PySense
const applicationName = 'tp-lora-cooc'           // Nom de l'application du serveur TTN
const password = 'NNSXS.LDTJFVIAELHH2ZGWSAQOQOVSGWJVJTBWQAVW5MA.2JBE65Y7WETTEYU5TLD4TOYLC6CH3UI3LHG4FSYO4KRH56BS3R6A'
const table_name = 'datas'                       // Nom de la table de la base de donnée

const website_port = 3000

//Connexion au serveur
const client = mqtt.connect(connectUrl, { // Connexion au serveur TTN
  clientId,
  clean: true,
  connectTimeout: 4000,
  username: `${applicationName}@ttn`,
  password: password,
  reconnectPeriod: 1000,
})

const topic = `v3/${applicationName}@ttn/devices/eui-${pyCard_id}/up` // topic sur lequel TTN publie les données

client.on('connect', () => {
  // Evenement lancé lors de la connexion vers le serveur TTN
  console.log('Successfully connected to the server')
  client.subscribe([topic], () => { // On s'abonne à un topic pour permettre de recevoir les messages que le serveur enverra
      console.log('Successfully subscribed to the topic : ' + topic)
  })
})

// Route pour récupérer les données depuis la base de données et les servir en HTML
app.get('/', async (req, res) => {
  try {
    // Connexion à la base de donnée
    const con = await sql.connect(config)
        .then(() => {console.log("Successfully connected to the SQL database")})
        .catch(() => {console.log("An error has occured during the connection to the database")})

    const datas = await fetchAllData(sql, table_name)

    // Formater les données en HTML
    res.render('index.ejs', {
        data: datas,
        filteringData: datas,
        extraction: extraction,
        functions: functions
    })
  } catch (err) {
    console.error('Erreur lors de la récupération des données :', err)
    res.status(500).send('Erreur serveur')
  } finally {
    await sql.close()
      .then(() => {
        console.log("SQL database sucessfully closed\n ----------------------------")
    })
  }
})

client.on('message', (topic, payload) => {
  ttn.insert(sql, payload)
})

// Démarrer le serveur sur le port 3000
app.listen(website_port, () => {
  console.log(`Serveur démarré sur le port ${website_port}`)
})