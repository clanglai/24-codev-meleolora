// We have a dateTime format in the SQL database and we want to convert it into a date format
function dateTimeToDate(dateTime) {
    let parts  = dateTime.split(' ')

    let day    = parseInt(parts[2])
    let month  = parts[1]
    let year   = parts[3]

    switch (month) {
        case 'Jan': { month = 1; break }
        case 'Feb': { month = 2; break }
        case 'Mar': { month = 3; break }
        case 'Apr': { month = 4; break }
        case 'May': { month = 5; break }
        case 'Jun': { month = 6; break }
        case 'Jul': { month = 7; break }
        case 'Aug': { month = 8; break }
        case 'Sep': { month = 9; break }
        case 'Oct': { month = 10; break }
        case 'Nov': { month = 11; break }
        case 'Dec': { month = 12; break }
        default: {break}
    }
    month = parseInt(month)
    year  = parseInt(year)

    return {
        day   : day,
        month : month,
        year  : year
    }
}

// Interverts two elements of a fiven array
function intervertArray(array, index1, index2) {
    let temp = array[index1]
    array[index1] = array[index2]
    array[index2] = temp
}

// Returns true if dateTime1 happens before dateTime2
function compareDate(dateTime_data1, dateTime_data2) {
    if (dateTime_data2.year > dateTime_data1.year) {
        return true
    }

    if (dateTime_data2.year == dateTime_data1.year && dateTime_data2.month > dateTime_data1.month) {
        return true
    }
    
    if (dateTime_data2.year == dateTime_data1.year && dateTime_data2.month == dateTime_data1.month && dateTime_data2.day >= dateTime_data1.day) {
        return true
    }

    return false
}

// Function to sort a dateTime array
function sortDateTimeArray(dateTime) {
    for (let i=0; i<dateTime.length - 1; i++) {
        if (compareDate(dateTime[i+1], dateTime[i]))
            intervertArray(dateTime, i+1, i)
    }

    return dateTime
}

// Function to sort all the data regards to the date (in case theres a data before another one)
function sortArrayData(arrayData) {
    let date = arrayData.dateArray
    let brig = arrayData.brightnessArray
    let humi = arrayData.humidityArray
    let pres = arrayData.pressureArray
    let rain = arrayData.rainfallArray
    let temp = arrayData.temperatureArray
    let wind = arrayData.windSpeedArray

    for (let i=0; i<date.length - 1; i++) {
        if (compareDate(date[i+1], date[i])) {
            intervertArray(date, i, i+1)
            intervertArray(brig, i, i+1)
            intervertArray(humi, i, i+1)
            intervertArray(pres, i, i+1)
            intervertArray(rain, i, i+1)
            intervertArray(temp, i, i+1)
            intervertArray(wind, i, i+1)
        }
    }
    return arrayData
}

// Function to convert the data into an array
function dataToArray(name, str) {
    const newArray = []

    name.forEach(element => {
        newArray.push(element[str])
    });

    return newArray
}

// Function to cut data between two given dates in the format 'DD-MM-YYYY'
function cutDataArray(dataArray, date1, date2) {
    if (dataArray.length == 0) {        
        return {
            dateArray         : [],
            brightnessArray   : [],
            humidityArray     : [],
            pressureArray     : [],
            rainfallArray     : [],
            temperatureArray  : [],
            windSpeedArray    : [],
        }
    }

    let split_1 = date1.split('-')
    let split_2 = date2.split('-')

    let date_1 = {
        day   : parseInt(split_1[0]),
        month : parseInt(split_1[1]),
        year  : parseInt(split_1[2])
    }

    let date_2 = {
        day   : parseInt(split_2[0]),
        month : parseInt(split_2[1]),
        year  : parseInt(split_2[2])
    }


    let date = dataArray.dateArray
    let index1 = 0
    let index2 = 0

    for (let i=0; i < date.length; i++) {
        if (compareDate(date_1, date[i])) {
            index1 = i
            break
        }
    }

    for (let i=0; i<date.length; i++) {
        if (compareDate(date[i], date_2)) {
            index2 = i+1
        }
    }
    
    dateArray        = dataArray.dateArray.slice(index1, index2)
    brightnessArray  = dataArray.brightnessArray.slice(index1, index2)
    humidityArray    = dataArray.humidityArray.slice(index1, index2)
    pressureArray    = dataArray.pressureArray.slice(index1, index2)
    rainfallArray    = dataArray.rainfallArray.slice(index1, index2)
    temperatureArray = dataArray.temperatureArray.slice(index1, index2)
    windSpeedArray   = dataArray.windSpeedArray.slice(index1, index2)

    return {
        dateArray         : dateArray,
        brightnessArray   : brightnessArray,
        humidityArray     : humidityArray,
        pressureArray     : pressureArray,
        rainfallArray     : rainfallArray,
        temperatureArray  : temperatureArray,
        windSpeedArray    : windSpeedArray,
    }
}

// Function to convert a dateTime List into a List
// Ex:
// dateTime : [{ brightness: 15 }, { brightness: 20 }, ...]
// dateTimeToList(dateTime) -> [15, 20, ...]
function dateTimeToList(dateTime) {
    let dateTimeArray = []

    dateTime.array.forEach(element => {
        dateTimeArray.push(element.dateTime)
    });

    return dateTimeArray
}

// Function to convert a dateArray into a StringArray (used to display it on the Xaxis of the graph)
function dateArrayToStringArray(dateArray) {
    let array = []
    dateArray.forEach((element) => {
        let tempStr = ''
        tempStr += element.day + '/'
        tempStr += element.month + '/'
        tempStr += element.year

        array.push(tempStr)
    })

    return array
}

// Function to create the graph with unfiltered data
function makeGraph(data) {
    const dateValues     = dateArrayToStringArray(data.dateArray)
    
    const brightnessValues  = data.brightnessArray
    const humidityValues    = data.humidityArray
    const pressureValues    = data.pressureArray
    const rainfallValues    = data.rainfallArray
    const temperatureValues = data.temperatureArray
    const windSpeedValues   = data.windSpeedArray

    return new Chart("graph", {
        type: "line",
        data: {
            labels: dateValues,
            datasets: [
                {
                    label: "Luminosité (Lux)",
                    backgroundColor: "rgba(255, 255, 50, 0.3)",
                    borderColor: "rgba(255, 255, 50, 0.8)",
                    data: brightnessValues,
                    min: 0,
                },
                {   
                    label: 'Humidité (%)',
                    backgroundColor:"rgba(255,102,255,0.3)",
                    borderColor: "rgba(255,102,255, 0.8)",
                    data: humidityValues,
                    min: 0,
                },
                {
                    label: "Pression (Pa)",
                    backgroundColor: "rgba(50, 255, 255, 0.3)",
                    borderColor: "rgba(50, 255, 255, 0.8)",
                    data: pressureValues,
                    min: 0
                },
                {
                    label: "Pluviométrie (mm²)",
                    backgroundColor: "rgba(102, 102, 255, 0.3)",
                    borderColor: "rgba(102, 102, 255, 0.8)",
                    data: rainfallValues,
                    min: 0,
                },
                {
                    label: "Température (°C)",
                    backgroundColor: "rgba(210, 13, 13, 0.3)",
                    borderColor: "rgba(210, 13, 13, 0.8)",
                    data: temperatureValues,
                    min: 0,
                },
                {
                    label: "Vitesse du vent (m/s)",
                    backgroundColor: "rgba(255, 255, 255, 0.3)",
                    borderColor: "rgba(255, 255, 255, 0.8)",
                    data: windSpeedValues,
                    min: 0
                }
            ],
        }
    });
}

// Function to update the graph with new data
function updateGraphXAxis(data, graph) {
    let dateValues     = dateArrayToStringArray(data.dateArray)

    graph.config.data.labels = dateValues

    replaceDataInGraph(data, graph)

    graph.update()
}

function replaceDataInGraph(data, graph) {
    for (dataset in graph.config.data.datasets) {
        if (dataset.label == "Luminosité") graph.config.data.datasets[findIndex(graph, "Luminosité")].data = data.brightnessArray
        if (dataset.label == "Humidité") graph.config.data.datasets[findIndex(graph, "Humidité")].data = data.humidityArray
        if (dataset.label == "Pression") graph.config.data.datasets[findIndex(graph, "Pression")].data = data.pressureArray
        if (dataset.label == "Pluviométrie") graph.config.data.datasets[findIndex(graph, "Pluviométrie")].data = data.rainfallArray
        if (dataset.label == "Température") graph.config.data.datasets[findIndex(graph, "Température")].data = data.temperatureArray
        if (dataset.label == "Vitesse du vent") graph.config.data.datasets[findIndex(graph, "Vitesse du vent")].data = data.windSpeedArray
    }
}