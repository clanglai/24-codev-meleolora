brigGrid = document.getElementsByClassName('brig-grid')[0]
humiGrid = document.getElementsByClassName('humi-grid')[0]
presGrid = document.getElementsByClassName('pres-grid')[0]
rainGrid = document.getElementsByClassName('rain-grid')[0]
tempGrid = document.getElementsByClassName('temp-grid')[0]
windGrid = document.getElementsByClassName('wind-grid')[0]

function findIndex(graph, name) {
    let datasets = graph.config.data.datasets

    for (let i=0; i<datasets.length; i++) {
        if (datasets[i].label == name) return i
    }

    return -1
}

function addToList(list, index, element) {
    let newlist = []

    for (let i=0; i<list.length; i++) {
        if (i == index) {
            newlist.push(element)
        }

        newlist.push(list[i])
    }

    return newlist
}

function removeToList(list, index) {
    let newlist = []

    for (let i=0; i<list.length; i++) {
        if (i != index) {
            newlist.push(list[i])
        }
    }

    return newlist
}

function filtBrig(datas, graph) {
    const index = findIndex(graph, "Luminosité (Lux)")
    let datasets = graph.config.data.datasets
    
    if (index == -1) {
        graph.config.data.datasets.push(datas.brightness)
    } else {
        datas.brightness = datasets[index]
        graph.config.data.datasets = removeToList(datasets, index)
    }

    graph.update()
}

function filtHumi(datas, graph) {
    const index = findIndex(graph, "Humidité (%)")
    let datasets = graph.config.data.datasets
    
    if (index == -1) {
        graph.config.data.datasets.push(datas.humidity)
    } else {
        datas.humidity = datasets[index]
        graph.config.data.datasets = removeToList(datasets, index)
    }

    graph.update()
}

function filtPres(datas, graph) {
    const index = findIndex(graph, "Pression (Pa)")
    let datasets = graph.config.data.datasets
    
    if (index == -1) {
        graph.config.data.datasets.push(datas.pressure)
    } else {
        datas.pressure = datasets[index]
        graph.config.data.datasets = removeToList(datasets, index)
    }

    graph.update()
}

function filtRain(datas, graph) {
    const index = findIndex(graph, "Pluviométrie (mm²)")
    let datasets = graph.config.data.datasets
    
    if (index == -1) {
        graph.config.data.datasets.push(datas.rainfall)
    } else {
        datas.rainfall = datasets[index]
        graph.config.data.datasets = removeToList(datasets, index)
    }

    graph.update()
}

function filtTemp(datas, graph) {
    const index = findIndex(graph, "Température (°C)")
    let datasets = graph.config.data.datasets
    
    if (index == -1) {
        graph.config.data.datasets.push(datas.temperature)
    } else {
        datas.temperature = datasets[index]
        graph.config.data.datasets = removeToList(datasets, index)
    }

    graph.update()
}

function filtWind(datas, graph) {
    const index = findIndex(graph, "Vitesse du vent (m/s)")
    let datasets = graph.config.data.datasets
    
    if (index == -1) {
        graph.config.data.datasets.push(datas.windSpeed)
    } else {
        datas.windSpeed = datasets[index]
        graph.config.data.datasets = removeToList(datasets, index)
    }

    graph.update()
}

let datas = {
    brightness  : {},
    humidity    : {},
    pressure    : {},
    rainfall    : {},
    temperature : {},
    windSpeed   : {}
}

// Buttons Event Listeners
brigGrid.addEventListener('click', () => {
    brigGrid.classList.toggle('filtclick')
    
    filtBrig(datas, graph)
})

humiGrid.addEventListener('click', () => {
    humiGrid.classList.toggle('filtclick')
    
    filtHumi(datas, graph)
})

presGrid.addEventListener('click', () => {
    presGrid.classList.toggle('filtclick')

    filtPres(datas, graph)
})

rainGrid.addEventListener('click', () => {
    rainGrid.classList.toggle('filtclick')

    filtRain(datas, graph)
})

tempGrid.addEventListener('click', () => {
    tempGrid.classList.toggle('filtclick')

    filtTemp(datas, graph)
})

windGrid.addEventListener('click', () => {
    windGrid.classList.toggle('filtclick')

    filtWind(datas, graph)
})