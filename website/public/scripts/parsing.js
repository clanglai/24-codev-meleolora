function getDate(json) {
    let list = json
        .split('dateArray&#34;:[')[1]
        .split('],&#34')[0]
        .split('&#34')

    let array = []

    for (let i=2; i < list.length - 4; i=i+6) {
        let day   = list[i]
            .split(':')[1]
            .split(',')[0]
        
        let month = list[i+2]
            .split(':')[1]
            .split(',')[0]
        
        let year  = list[i+4]
            .split(':')[1]
            .split('}')[0]

        let temp = {
            day   : parseInt(day),
            month : parseInt(month),
            year  : parseInt(year)
        }

        array.push(temp)
    }

    return array
}

function getBrightness(json) {
    let list = json.split('brightnessArray&#34;:')[1]

    list = list
        .split(',&#34')[0]
        .slice(1, list.length - 1)
        .split(',')

    let array = []

    for (let i=0; i < list.length; i++) {
        array.push(parseFloat(list[i]))
    }

    return array
}

function getHumidity(json) {
    let list = json.split('humidityArray&#34;:')[1]

    list = list
        .split(',&#34')[0]
        .slice(1, list.length - 1)
        .split(',')

    let array = []

    for (let i=0; i < list.length; i++) {
        array.push(parseFloat(list[i]))
    }

    return array
}

function getPressure(json) {
    let list = json.split('pressureArray&#34;:')[1]

    list = list
        .split(',&#34')[0]
        .slice(1, list.length - 1)
        .split(',')

    let array = []

    for (let i=0; i < list.length; i++) {
        array.push(parseInt(list[i]))
    }

    return array
}

function getRainfall(json) {
    let list = json.split('rainfallArray&#34;:')[1]

    list = list
        .split(',&#34')[0]
        .slice(1, list.length - 1)
        .split(',')

    let array = []

    for (let i=0; i < list.length; i++) {
        array.push(parseFloat(list[i]))
    }

    return array
}

function getTemperature(json) {
    let list = json.split('temperatureArray&#34;:')[1]

    list = list
        .split(',&#34')[0]
        .slice(1, list.length - 1)
        .split(',')

    let array = []

    for (let i=0; i < list.length; i++) {
        array.push(parseFloat(list[i]))
    }
    
    return array
}

function getWindSpeed(json) {
    let list = json.split('windSpeedArray&#34;:')[1]

    list = list
        .split(',&#34')[0]
        .slice(1, list.length - 1)
        .split(',')

    let array = []

    for (let i=0; i < list.length; i++) {
        array.push(parseFloat(list[i]))
    }

    return array
}

function getData(json) {
    return {
        dateArray        : getDate(json),
        brightnessArray  : getBrightness(json),
        humidityArray    : getHumidity(json),
        pressureArray    : getPressure(json),
        rainfallArray    : getRainfall(json),
        temperatureArray : getTemperature(json),
        windSpeedArray   : getWindSpeed(json)
    }
}