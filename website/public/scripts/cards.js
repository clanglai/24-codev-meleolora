var select = document.getElementsByClassName("selection")[0]

var hist = document.getElementsByClassName("history--page")[0]

// Function to make appear the history page when we click on a data-box
function show() {
    hist.style.animation = 'appear 0.5s'
    select.style.animation = 'appear 0.5s'

    hist.style.visibility = 'visible'
    select.style.visibility = 'visible'
}

// Function to make disappear the history page when we click on the X of the history page
function disappear() {
    hist.style.animation = 'fade 0.5s'
    select.style.visibility = 'hidden'

    setTimeout(() => {
        hist.style.visibility = 'hidden'
    }, 480)
}