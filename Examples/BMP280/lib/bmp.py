from machine import I2C, Pin
import time
class BMP280:
    def __init__(self, i2c, addr=0x76):
        self.i2c = i2c
        self.addr = addr
        self.calibrate()
        
    def calibrate(self):
        # Lire les données d'étalonnage stockées dans le BMP280
        regs = self.i2c.readfrom_mem(self.addr, 0x88, 24)
        self.dig_T1 = self.u16_le(regs, 0)
        self.dig_T2 = self.s16_le(regs, 2)
        self.dig_T3 = self.s16_le(regs, 4)
        self.dig_P1 = self.u16_le(regs, 6)
        self.dig_P2 = self.s16_le(regs, 8)
        self.dig_P3 = self.s16_le(regs, 10)
        self.dig_P4 = self.s16_le(regs, 12)
        self.dig_P5 = self.s16_le(regs, 14)
        self.dig_P6 = self.s16_le(regs, 16)
        self.dig_P7 = self.s16_le(regs, 18)
        self.dig_P8 = self.s16_le(regs, 20)
        self.dig_P9 = self.s16_le(regs, 22)

    def u16_le(self, bytes, index):
        return bytes[index] + (bytes[index + 1] << 8)

    def s16_le(self, bytes, index):
        result = self.u16_le(bytes, index)
        if result > 32767:
            result -= 65536
        return result
        def read(self):
        # Écrire le registre de contrôle pour configurer le capteur
        self.i2c.writeto_mem(self.addr, 0xF4, b'\x27')  # Température et pression avec oversampling x1
        self.i2c.writeto_mem(self.addr, 0xF5, b'\x10')  # 16ms de standby, IIR filter off
        time.sleep(0.5)  # Laisser le temps au capteur de mesurer

        # Lire les données de température et de pression
        data = self.i2c.readfrom_mem(self.addr, 0xF7, 6)
        adc_p = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
        adc_t = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)

        # Conversion des valeurs ADC en température et pression
        temp = self.compensate_T(adc_t)
        pres = self.compensate_P(adc_p)
        return (temp, pres)

    def compensate_T(self, adc_T):
        var1 = (((adc_T >> 3) - (self.dig_T1 << 1)) * self.dig_T2) >> 11
        var2 = (((((adc_T >> 4) - self.dig_T1) * ((adc_T >> 4) - self.dig_T1)) >> 12) * self.dig_T3) >> 14
        self.t_fine = var1 + var2
        T = (self.t_fine * 5 + 128) >> 8
        return T / 100.0

    def compensate_P(self, adc_P):
        var1 = self.t_fine - 128000
        var2 = var1 * var1 * self.dig_P6
        var2 = var2 + ((var1 * self.dig_P5) << 17)
        var2 = var2 + (self.dig_P4 << 35)
        var1 = ((var1 * var1 * self.dig_P3) >> 8) + ((var1 * self.dig_P2) << 12)
        var1 = (((1 << 47) + var1) * self.dig_P1) >> 33
        if var1 == 0:
            return 0  # Éviter la division par zéro
        p = 1048576 - adc_P
        p = (((p << 31) - var2) * 3125) // var1
        var1 = (self.dig_P9 * (p >> 13) * (p >> 13)) >> 25
        var2 = (self.dig_P8 * p) >> 19
        p = ((p + var1 + var2) >> 8) + (self.dig_P7 << 4)
        return p / 25600.0
        i2c = I2C(0, I2C.MASTER, baudrate=400000)
        bmp280 = BMP280(i2c)

temp, pres = bmp280.read()
print('Temperature:', temp, 'C')
print('Pressure:', pres, 'hPa')