import time
import pycom
import ubinascii
from machine import enable_irq, disable_irq, Pin, I2C
from network import LoRa
import socket
import struct
import binascii
import bme280

#################Définition des fonctions#########################

def connect_to_ttn(lora_object):
    """Receives a lora object and tries to join"""
    # join a network using OTAA (Over the Air Activation)
    lora_object.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
    #lora_object.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0, dr=0)
    pycom.rgbled(0xff00ff)
    # wait until the module has joined the network
    while not lora_object.has_joined():
        #pass
        time.sleep(5)
        print('Not yet joined...')
        lora.nvram_erase()

def read_bmp280():
    """Reads data from BMP280 sensor"""
    # Initialize the I2C bus
    i2c = I2C(0, pins=('P10', 'P11'))
    bmp = bme280.BME280(i2c=i2c)

    # Read the data from the sensor
    temperature, pressure, humidity = bmp.values

    return temperature, pressure, humidity

################Code exécuté par la carte########################

# Définition des paramètres de la carte
app_eui = ubinascii.unhexlify('0000000000000000')
app_key = ubinascii.unhexlify('D5CD5DA255EE49683DE7E58B6EC7C14D')
dev_eui = ubinascii.unhexlify('70B3D54991FD41A2')

# Défiition des paramètres de communication LoRa
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, adr=True, device_class = LoRa.CLASS_C)
lora.nvram_restore() #if there is nothing to restore it will return a False
print(binascii.hexlify(lora.mac()).upper())

# Connexion au serveur TTN et changement de la couleur de la LED en vert
connect_to_ttn(lora)
print("CONNECTED!!")
pycom.rgbled(0x00ff00)

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# make the socket blocking
s.setblocking(True)

while True:
    temperature, pressure, humidity = read_bmp280()
    print('Temperature:', temperature)
    print('Pressure:', pressure)
    print('Humidity:', humidity)

    # Your logic for LED indication or any other action here

    # Prepare the data packet
    data = struct.pack('<fff', temperature, pressure, humidity)

    # Send the data packet
    s.send(data)

    # Sleep for desired duration before sending the next packet
    time.sleep(300)  # Example: Sleep for 5 minutes
