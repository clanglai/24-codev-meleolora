import time
import ubinascii
import machine
from network import LoRa
import socket

# Définition des paramètres de la carte
app_eui = ubinascii.unhexlify('0000000000000000')
app_key = ubinascii.unhexlify('D5CD5DA255EE49683DE7E58B6EC7C14D')
dev_eui = ubinascii.unhexlify('70B3D54991FD41A2')

# Défiition des paramètres de communication LoRa
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, adr=True, device_class=LoRa.CLASS_C)
lora.nvram_restore()  # si rien à restaurer, renvoie False

def connect_to_ttn():
    """Rejoint le réseau LoRaWAN via OTAA."""
    # Tente de rejoindre le réseau
    lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
    while not lora.has_joined():
        time.sleep(5)
        print('Not yet joined...')
        lora.nvram_erase()

# Connexion au serveur TTN
connect_to_ttn()
print("CONNECTED!!")

# Création d'une socket LoRa
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# Définition du taux de données LoRaWAN
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# Boucle principale
while True:
    # Lecture de la valeur de pluviométrie
    rain_value = machine.ADC(machine.Pin('P16'))
    rain_value.atten(machine.ADC.ATTN_11DB)
    rain_measurement = rain_value.read()

    # Envoi de la valeur à TTN
    s.send(struct.pack('f', rain_measurement))
    print('Rain measurement sent:', rain_measurement)

    # Attente avant la prochaine lecture et envoi
    time.sleep(300)
