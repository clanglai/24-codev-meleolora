#works 19/09/2022
from network import LoRa
import socket
import time
import struct
import ubinascii
import binascii
import pycom

def connect_to_ttn(lora_object):
    """Receives a lora object and tries to join"""
    # join a network using OTAA (Over the Air Activation)
    lora_object.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
    #lora_object.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0, dr=0)
    pycom.rgbled(0xff00ff)
    # wait until the module has joined the network
    while not lora_object.has_joined():
        #pass
        time.sleep(5)
        print('Not yet joined...')
        lora.nvram_erase()


pycom.heartbeat(False)

app_eui = ubinascii.unhexlify('0000000000000000')

app_key = ubinascii.unhexlify('85158ECB24ECB9FAF2C9F25A4D28E88D')
dev_eui = ubinascii.unhexlify('70B3D54990A5B8D3')

lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, adr=True, device_class = LoRa.CLASS_C)
lora.nvram_restore() #if there is nothing to restore it will return a False
print(binascii.hexlify(lora.mac()).upper())

connect_to_ttn(lora)

print("CONNECTED!!")
pycom.rgbled(0x00ff00)

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# make the socket blocking
s.setblocking(True)


for i in range (20):
    pkt = b'PKT ' + bytes([i])
    print('Sending:', pkt)
    s.send(pkt)
    time.sleep(4)
    #rx, port = s.recvfrom(256)
    #if rx:
    #    print('Received: {}, on port: {}'.format(rx, port))
    time.sleep(6)
