
# example from https://docs.pycom.io/tutorials/networks/lora/module-module/

from network import LoRa
import socket
import time

# Please pick the region that matches where you are using the device

lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868, sf=7)
print('Coding rate {}'.format(lora.coding_rate()))

#lora.sf(12)
print('SF {}'.format(lora.sf()))

s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

s.setblocking(False)
i = 0
while True:
    if s.recv(64) == b'PingA':
        print('je recois Ping du Node A')
        print(lora.stats())
        #print(lora.coding_rate())
        #s.send('Pong')
        print('Pong {}'.format(i))
        i = i+1
    time.sleep(5)
