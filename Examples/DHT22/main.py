import time
import pycom
from machine import Pin
from dht import DHT22

# Initialize DHT22 on pin P9
sensor = DHT22(Pin('P9', mode=Pin.OPEN_DRAIN))

# Disable the heartbeat LED to use the RGB LED
pycom.heartbeat(False)

while True:
    # Take a measurement
    sensor.measure()

    # Read temperature and humidity from the sensor
    temperature = sensor.temperature()
    humidity = sensor.humidity()
# Print the results
    print('Temp:', temperature)
    print('RH:', humidity)

    # Change the RGB LED color based on the temperature
    if temperature > 25:
        pycom.rgbled(0xff0000)  # Red for temperatures above 25 degrees Celsius
    else:
        pycom.rgbled(0x00ff00)  # Green otherwise

    # Wait 2.5 seconds, then switch LED to blue, wait another 2.5 seconds
    time.sleep(2.5)
    pycom.rgbled(0x0000ff)
    time.sleep(2.5)