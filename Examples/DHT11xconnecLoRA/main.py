import time
import pycom
import ubinascii
from machine import enable_irq, disable_irq, Pin
from dht import DTH
from network import LoRa
import socket
import struct
import binascii

#################Définiton des fonctions#########################

def connect_to_ttn(lora_object):
    """Receives a lora object and tries to join"""
    # join a network using OTAA (Over the Air Activation)
    lora_object.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
    #lora_object.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0, dr=0)
    pycom.rgbled(0xff00ff)
    # wait until the module has joined the network
    while not lora_object.has_joined():
        #pass
        time.sleep(5)
        print('Not yet joined...')
        lora.nvram_erase()


################Code exécuté par la carte########################

#Mettre votre n° de broche
th = DTH('P9')
pycom.heartbeat(False)

# Définition des paramètres de la carte
app_eui = ubinascii.unhexlify('0000000000000000')
app_key = ubinascii.unhexlify('D5CD5DA255EE49683DE7E58B6EC7C14D')
dev_eui = ubinascii.unhexlify('70B3D54991FD41A2')

# Défiition des paramètres de communication LoRa
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, adr=True, device_class = LoRa.CLASS_C)
lora.nvram_restore() #if there is nothing to restore it will return a False
print(binascii.hexlify(lora.mac()).upper())

# Connexion au serveur TTN et changement de la couleur de la LED en vert
connect_to_ttn(lora)
print("CONNECTED!!")
pycom.rgbled(0x00ff00)

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# make the socket blocking
s.setblocking(True)

while True:
    result = th.read()
    while not result.is_valid():
        time.sleep(.5)
        result = th.read()
    print('Temp:', result.temperature)
    print('RH:', result.humidity)

    if result.temperature > 25:
        pycom.rgbled(0xff0000)
    else:
        pycom.rgbled(0x00ff00)

    #5 secondes entre 2 lectures avec retour au bleu
    time.sleep(2.5)
    pycom.rgbled(0x0000ff)
    time.sleep(2.5)

#########################################################

#works 19/09/2022
# from network import LoRa
# import socket
# import time
# import struct
# import ubinascii
# import binascii
# import pycom

# def connect_to_ttn(lora_object):
# """Receives a lora object and tries to join"""
# # join a network using OTAA (Over the Air Activation)
# lora_object.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
# #lora_object.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0, dr=0)
# pycom.rgbled(0xff00ff)
# # wait until the module has joined the network
# while not lora_object.has_joined():
#     #pass
#     time.sleep(5)
#     print('Not yet joined...')
#     lora.nvram_erase()


# pycom.heartbeat(False)

# app_eui = ubinascii.unhexlify('0000000000000000')

# app_key = ubinascii.unhexlify('D5CD5DA255EE49683DE7E58B6EC7C14D')
# dev_eui = ubinascii.unhexlify('70B3D54991FD41A2')

# # Défiition des paramètres de communication LoRa
# lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, adr=True, device_class = LoRa.CLASS_C)
# lora.nvram_restore() #if there is nothing to restore it will return a False
# print(binascii.hexlify(lora.mac()).upper())

# connect_to_ttn(lora)

# print("CONNECTED!!")
# pycom.rgbled(0x00ff00)

# # create a LoRa socket
# s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# # set the LoRaWAN data rate
# s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# # make the socket blocking
# s.setblocking(True)

# Envoi des données

for i in range (20):
    pkt = b'PKT ' + bytes([i])
    print('Sending:', pkt)
    s.send(pkt)
    time.sleep(4)
    #rx, port = s.recvfrom(256)
    #if rx:
    #    print('Received: {}, on port: {}'.format(rx, port))
    time.sleep(6)


#while True: # Boucle infinie permettant de récolter les données et de les envoyer sur TTN
    ## Stockage des données mesurées dans des variables 
    #temp = float(str(dht.temperature()))
    #temp_arrondi = round(temp,1)
    # humidity=float(str(dht.humidity()))
    # humidity_arrondi=round(humidity)
    # pression=float(str(press.pressure()))
    # pression_arrondi=round(pression)
    # lumin=float(str(li.lux()))
    # lumin_arrondi=round(lumin)

    # # Envoi des données à TTN sous la forme d'une seule chaîne de caractères, séparées par des virgules
    # s.send( str(temp_arrondi) +','+ str(humidity_arrondi) + ',' + str(pression_arrondi) +',' + str(lumin_arrondi))

    # # Envoi des données suivantes après 5 minutes
    # time.sleep(300)
