# Carnet de bord  du projet MeteoLoRa

## R1 07/02/2024

<!-- Indiquez ici ce que vous avez retenu de la réunion sous forme de todolist -->

- [ ] Explication du projet et discussion sur les tâches à accomplir
- [ ] Clonage du repository
- [ ] Visionnage de la vidéo sur le LoRaWAN
- [ ] Passage au fablab pour découvrir le matériel que nous utiliserons


## R2 22/03/2024

- interface appli, afficher des données grace au serveur TTN
- Utilisez MQTT, faire marcher ce qui marchait l'année dernière
- Regarder capteur et les programmers pour récupérer les données 
- DHT11 DHT22 comparer les valeurs de température avec les la carte donnée de base (mettre le code la carte l'année dernière)
- #Entre la carte et le serveur TTN ca change pas et entre le capteur et la carte ca change

-Géraud maitrise git


## R4 03/04/2024

- fin des tp Lopy4
- tests de la carte avec le dht 11 : temp et humidité bien reçues mais pas très précises.
- essayer avec le dht 22, voire avec d'autres capteurs.
- envoi vers la base de données fonctionnel, voir avec Matthieu pour la liaison des différentes bases de données. (ou la création d'une autre intermédiaire ?)

## R5 17/04/2024
- avancée du planning : retard remarqué
- difficultées pour la démo : défaut de branchement des capteurs --> non-fonctionnement de la carte (retour au code minimal traffic lights)
- réussite pour la relance du code du DHT11
- ouverture du code pysense de l'annéee dernière (pour envoi vers Lora) : https://gitlab.imt-atlantique.fr/23-codev-meteolora/lora/-/blob/main/code/programme_carte_pysense/main.py?ref_type=heads
- ce matin : remonter les infos du DHT11 vers la plateforme TTN (besoin du code ci dessus + code DHT11) : connexion TTN, initialisation des capteurs, lecture et mise en forme des données puis envoi. [NB: tester avant que le code OTAA connexion fonctionne avant de le mettre ]
- DHT22 au fablab? voir sinon avec le BMP280 (pour la porchaine fois)
- commencer conception de la boîte
- intégrer avec l'interface pour faire la démo complète pour le dht11.


## R6 03/05/2024
- avancement du code permettant l'envoi de données du DHT11 vers le serveur TTN.
- puis envoi des données récupérées sur le serveur vers l'application client
- début de la configuration de la boîte
- pour la prochaine fois : élaboration des codes pour le BMP280 (pression, température) et du SEN0575 (pluviométrie) pour envoyer les données vers le serveur TTN
- trouver les modalités de branchement de ces capteurs sur la carte
- test de ces codes et si validation : démo complète pour envoyer les données vers l'application.
- avancement de la rédaction du rapport
- avancement de la configuration de la boîte


---
